extends Reference
class_name QuadTree

## Generic quadtree that stores data objects under unique position
##
## Be aware that only one object could be stored at equal vector positions,
## otherwise behavior is undefined

# todo: Should they all be refcounted resources?
# todo: Get rid of recursion where possible
# todo: Faster way of iterating over all elements

## How many points can be stored in single node before subdividing
const c_capacity := 4

# todo: Do we really care about origin? We might only store size vector instead
var m_bounds: Rect2
var m_points: PoolVector2Array
var m_elements: Array

## Children
var m_north_west: QuadTree = null
var m_north_east: QuadTree = null
var m_south_west: QuadTree = null
var m_south_east: QuadTree = null


class Item extends Reference:
    var m_point: Vector2
    var m_element

    func _init(point: Vector2, element) -> void:
        m_point = point
        m_element = element


func _init(bounds: Rect2) -> void:
    m_bounds = bounds
    # todo: If we want to preallocate those we need to store element count independently
    # m_points.resize(c_capacity)
    # m_elements.resize(c_capacity)


func insert(point: Vector2, element) -> void:
    assert(m_bounds.has_point(point))

    if m_points.size() < c_capacity and m_north_west == null:
        m_points.push_back(point)
        m_elements.push_back(element)
        return

    if m_north_west == null:
        _subdivide()

    if m_north_west.m_bounds.has_point(point):
        m_north_west.insert(point, element)
    elif m_north_east.m_bounds.has_point(point):
        m_north_east.insert(point, element)
    elif m_south_west.m_bounds.has_point(point):
        m_south_west.insert(point, element)
    else:
        m_south_east.insert(point, element)


func has_point(point: Vector2) -> bool:
    assert(m_bounds.has_point(point))

    for i in m_points:
        if point == m_points[i]:
            return true

    assert(m_north_west != null)

    if m_north_west.m_bounds.has_point(point):
        return m_north_west.has_point(point)
    elif m_north_east.m_bounds.has_point(point):
        return m_north_east.has_point(point)
    elif m_south_west.m_bounds.has_point(point):
        return m_south_west.has_point(point)
    else:
        return m_south_east.has_point(point)


func erase(point: Vector2) -> void:
    assert(m_bounds.has_point(point))

    for i in m_points:
        if point == m_points[i]:
            m_points.remove(i)
            m_elements.remove(i)
            return

    assert(m_north_west != null)

    if m_north_west.m_bounds.has_point(point):
        m_north_west.erase(point)
    elif m_north_east.m_bounds.has_point(point):
        m_north_east.erase(point)
    elif m_south_west.m_bounds.has_point(point):
        m_south_west.erase(point)
    else:
        m_south_east.erase(point)

    # todo: Should we do it every time? User-called shrink function
    #       might be more flexible in case we need contant erasure of items
    if m_north_west.m_points.empty() and \
            m_north_east.m_points.empty() and \
            m_south_west.m_points.empty() and \
            m_south_east.m_points.empty():
        m_north_west = null
        m_north_east = null
        m_south_west = null
        m_south_east = null


## Get array of all points enclosed by given rect that have element stored at them
func query_points(rect: Rect2) -> PoolVector2Array:
    var result := PoolVector2Array()
    if not m_bounds.intersects(rect):
        return result

    for point in m_points:
        if rect.has_point(point):
            result.push_back(point)

    if m_north_west != null:
        result.append_array(m_north_west.query_points(rect))
        result.append_array(m_north_east.query_points(rect))
        result.append_array(m_south_west.query_points(rect))
        result.append_array(m_south_east.query_points(rect))

    return result


## Get array of all elements at points enclosed by given rect
func query_elements(rect: Rect2) -> Array:
    var result := Array()
    if not m_bounds.intersects(rect):
        return result

    for i in m_points.size():
        if rect.has_point(m_points[i]):
            result.push_back(m_elements[i])

    if m_north_west != null:
        result.append_array(m_north_west.query_elements(rect))
        result.append_array(m_north_east.query_elements(rect))
        result.append_array(m_south_west.query_elements(rect))
        result.append_array(m_south_east.query_elements(rect))

    return result


## Get array of all items at points enclosed by given rect
##
## Retuned array has all items of type Item
func query_items(rect: Rect2) -> Array:
    var result := Array()
    if not m_bounds.intersects(rect):
        return result

    for i in m_points.size():
        var point = m_points[i]
        if rect.has_point(point):
            result.push_back(Item.new(point, m_elements[i]))

    if m_north_west != null:
        result.append_array(m_north_west.query_items(rect))
        result.append_array(m_north_east.query_items(rect))
        result.append_array(m_south_west.query_items(rect))
        result.append_array(m_south_east.query_items(rect))

    return result


## Get element at point coordinates
##
## Given point should be present in quadtree
func element_at_point(point: Vector2):
    for i in m_points.size():
        if m_points[i] == point:
            return m_elements[i]

    assert(m_north_west != null)

    if m_north_west.m_bounds.has_point(point):
        return m_north_west.element_at_point(point)
    elif m_north_east.m_bounds.has_point(point):
        return m_north_east.element_at_point(point)
    elif m_south_west.m_bounds.has_point(point):
        return m_south_west.element_at_point(point)
    else:
        return m_south_east.element_at_point(point)


func _subdivide() -> void:
    assert(not is_zero_approx(m_bounds.size.x))

    var div_dim := m_bounds.size.x / 2
    var div_size := Vector2(div_dim, div_dim)

    m_north_west = get_script().new(Rect2(m_bounds.position, div_size))
    m_north_east = get_script().new(Rect2(
        Vector2(m_bounds.position.x + div_dim, m_bounds.position.y), div_size))
    m_south_west = get_script().new(Rect2(
        Vector2(m_bounds.position.x, m_bounds.position.y + div_dim), div_size))
    m_south_east = get_script().new(Rect2(
        Vector2(m_bounds.position.x + div_dim, m_bounds.position.y + div_dim), div_size))

    for i in m_points.size():
        var point := m_points[i]
        if m_north_west.m_bounds.has_point(point):
            m_north_west.insert(point, m_elements[i])
        elif m_north_east.m_bounds.has_point(point):
            m_north_east.insert(point, m_elements[i])
        elif m_south_west.m_bounds.has_point(point):
            m_south_west.insert(point, m_elements[i])
        else:
            m_south_east.insert(point, m_elements[i])

    # todo: Assign null instead? Could reduce memory footprint
    m_points.resize(0)
    m_elements.clear()
